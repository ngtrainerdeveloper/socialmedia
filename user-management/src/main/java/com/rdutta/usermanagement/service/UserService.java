package com.rdutta.usermanagement.service;

import com.rdutta.usermanagement.dto.UserRequest;

import java.util.List;

public interface UserService {
    void createUser(UserRequest userRequest);
    void updateUser(UserRequest userRequest, Long id);
}
