package com.rdutta.usermanagement.event;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import java.time.LocalDateTime;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class NewUserCreated {
    private Long _userId;
    private LocalDateTime createdAt;
}
