package com.rdutta.usermanagement.serviceimpl;

import com.rdutta.usermanagement.dto.UserRequest;
import com.rdutta.usermanagement.event.NewUserCreated;
import com.rdutta.usermanagement.event.UserUpdated;
import com.rdutta.usermanagement.mappers.UserMapping;
import com.rdutta.usermanagement.model.UserEntity;
import com.rdutta.usermanagement.repository.UserRepository;
import com.rdutta.usermanagement.service.UserService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Service;

import java.util.concurrent.ThreadLocalRandom;

@Service
@Slf4j
public class UserServiceImpl implements UserService {
    private final UserRepository userRepository;
    private final KafkaTemplate<String, NewUserCreated> kafkaNewUserTemplate;
    private final KafkaTemplate<String, UserUpdated> kafkaUpdateUserTemplate;
    private final UserMapping userMapper;

    @Autowired
    public UserServiceImpl(UserRepository userRepository,
                           KafkaTemplate<String, NewUserCreated> kafkaNewUserTemplate,KafkaTemplate<String, UserUpdated> kafkaUpdateUserTemplate, UserMapping userMapper) {
        this.userRepository = userRepository;
        this.kafkaNewUserTemplate = kafkaNewUserTemplate;
        this.kafkaUpdateUserTemplate = kafkaUpdateUserTemplate;
        this.userMapper = userMapper;
    }

    //Creation of New User Account
    @Override
    public void createUser(UserRequest userRequest) {
        UserEntity user = userMapper.toUserEntity(userRequest);
        Long newId = ThreadLocalRandom.current().nextLong(10000, 100000);
        user.setId(newId);
        user.onUserCreate();
        userRepository.save(user);
        log.info("User got created with id - {} ", user.getId());
        kafkaNewUserTemplate.send("user_created_topic", new NewUserCreated(user.getId(), user.getCreatedAt()));
    }

    //Update existing user
    @Override
    public void updateUser(UserRequest userRequest, Long id) {
        UserEntity existingUser = userRepository.findById(id).orElseThrow(() -> new IllegalArgumentException("User id not found in our data, please try again with correct user id."));

        existingUser.set_username(userRequest.get_username());
        existingUser.set_password(userRequest.get_password());
        existingUser.set_confirmPassword(userRequest.get_confirmPassword());
        existingUser.set_userEmail(userRequest.get_userEmail());
        existingUser.onUserUpdate();

        UserEntity updatedUser = userRepository.save(existingUser);
        userRepository.save(updatedUser);
        log.info("User got updated with id - {} ", updatedUser.getId());
        kafkaUpdateUserTemplate.send("user_updated_topic", new UserUpdated(updatedUser.getId(), updatedUser.getUpdatedAt()));
    }
}
