package com.rdutta.usermanagement.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;
import java.time.LocalDate;


@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class UserRequest {
    private Long id;
    @NotBlank(message = "Username is required.")
    private String _username;
    @NotBlank(message = "Password is required.")
    @Size(min = 6, max = 20, message = "Password length should be between 6 and 20 characters")
    private String _password;
    @NotBlank(message = "Confirm your password required.")
    @Size(min = 6, max = 20, message = "Password length should be between 6 and 20 characters")
    private String _confirmPassword;
    @NotBlank(message = "Email is required.")
    @Email(message = "Invalid email address.")
    private String _userEmail;
}
