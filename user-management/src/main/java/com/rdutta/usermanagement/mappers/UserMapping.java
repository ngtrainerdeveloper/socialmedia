package com.rdutta.usermanagement.mappers;

import com.rdutta.usermanagement.dto.UserRequest;
import com.rdutta.usermanagement.model.UserEntity;
import org.springframework.stereotype.Component;

@Component
public class UserMapping {
    public static UserEntity toUserEntity(UserRequest userRequest) {
        return UserEntity.builder()
                .id(userRequest.getId())
                ._username(userRequest.get_username())
                ._password(userRequest.get_password())
                ._confirmPassword(userRequest.get_confirmPassword())
                ._userEmail(userRequest.get_userEmail())
                .build();
    }

    public static UserRequest toUserRequest(UserEntity userEntity) {
        return UserRequest.builder()
                .id(userEntity.getId())
                ._username(userEntity.get_username())
                ._password(userEntity.get_password())
                ._confirmPassword(userEntity.get_confirmPassword())
                ._userEmail(userEntity.get_userEmail())
                .build();
    }
}
