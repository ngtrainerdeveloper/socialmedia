package com.rdutta.usermanagement.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.*;
import java.time.LocalDateTime;


@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@Entity
@Table(name = "user_management")
public class UserEntity {
    @Id
    private Long id;
    private String _username;
    private String _password;
    private String _confirmPassword;
    private String _userEmail;

    @Column(name = "created_at")
    @DateTimeFormat(pattern = "dd-MM-yyyy HH:mm:ss")
    private LocalDateTime createdAt;

    @Column(name = "updated_at")
    @DateTimeFormat(pattern = "dd-MM-yyyy HH:mm:ss")
    private LocalDateTime updatedAt;

    @PrePersist
    public void onUserCreate(){
        createdAt = LocalDateTime.now();
    }

    @PreUpdate
    public void onUserUpdate(){
        updatedAt = LocalDateTime.now();
    }
}
