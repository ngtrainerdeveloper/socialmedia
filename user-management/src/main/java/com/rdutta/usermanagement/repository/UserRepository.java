package com.rdutta.usermanagement.repository;

import com.rdutta.usermanagement.model.UserEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;


@Repository
public interface UserRepository extends JpaRepository<UserEntity, Long> {
 Optional<UserEntity> findById(Long id);
 UserEntity findBy_userEmail(String _userEmail);
}
