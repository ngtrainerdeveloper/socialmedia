package com.rdutta.postservice.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import java.time.LocalDateTime;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class PostDTO {
    private Long id;
    private String _userEmail;
    private String _title;
    private String _body;
    private LocalDateTime _postCreatedAt;
    private LocalDateTime _postDeletedAt;

}
