package com.rdutta.postservice.controller;

import com.rdutta.postservice.dto.PostDTO;
import com.rdutta.postservice.service.PostService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.ws.rs.Consumes;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import java.time.LocalDateTime;
import java.util.List;

@RestController
@RequestMapping(path = "/api/post")
public class PostController {
    private final PostService postService;

    @Autowired
    public PostController(PostService postService) {
        this.postService = postService;
    }

    @PostMapping("/generate")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public ResponseEntity<Void> createPost(@RequestBody PostDTO postDTO){

        postDTO.set_postCreatedAt(LocalDateTime.now());
        postService.createPost(postDTO);
        return new ResponseEntity<>(HttpStatus.CREATED);
    }

    @GetMapping("/AllPostOfUser")
    public ResponseEntity<List<PostDTO>> getAllPostByUserEmail(@RequestParam("email") String _userEmail){
        List<PostDTO> postDTO = postService.getAllPostsByUser(_userEmail);
        return new ResponseEntity<>(postDTO, HttpStatus.OK);
    }

   @GetMapping("/findOnePost")
    public ResponseEntity<PostDTO> getPostById(@RequestParam("id") Long id){
        PostDTO postDTO = postService.getPostById(id);
        return new ResponseEntity<>(postDTO, HttpStatus.OK);
    }

    @DeleteMapping("/remove")
    public ResponseEntity<Void> deletePost(@RequestParam("id") Long id, @RequestParam("email") String _userEmail){
        postService.deletePost(id, _userEmail);
        return new ResponseEntity<>(HttpStatus.OK);
    }
}
