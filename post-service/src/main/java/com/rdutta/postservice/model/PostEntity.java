package com.rdutta.postservice.model;

import com.rdutta.usermanagement.model.UserEntity;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.*;
import javax.validation.constraints.Pattern;
import java.time.LocalDateTime;
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@Entity
@Table(name = "posts_entity")
public class PostEntity {
    @Id
    private Long id;
    @ManyToOne(targetEntity = UserEntity.class, cascade = CascadeType.ALL)
    @JoinColumn(name = "user_id", referencedColumnName = "id")
    private UserEntity userEntity;

    @Column(name = "_userEmail")
    private String _userEmail;

    private String _title;
    @Column(columnDefinition = "TEXT")
    private String _body;
    @Column(name = "post_created_at")
    @DateTimeFormat(pattern = "dd-MM-yyyy HH:mm:ss")
    private LocalDateTime _postCreatedAt;
    @Column(name = "post_updated_at")
    @DateTimeFormat(pattern = "dd-MM-yyyy HH:mm:ss")
    private LocalDateTime _postDeletedAt;

    @PrePersist
    public void onPostCreate(){
        _postCreatedAt = LocalDateTime.now();
    }

    @PreUpdate
    public void onPostDeleted(){
        _postDeletedAt = LocalDateTime.now();
    }
}
