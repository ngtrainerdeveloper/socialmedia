package com.rdutta.postservice.serviceimpl;

import com.rdutta.postservice.dto.PostDTO;
import com.rdutta.postservice.events.NewPostCreatedEvent;
import com.rdutta.postservice.events.PostDeletedEvent;
import com.rdutta.postservice.mappers.PostMapper;
import com.rdutta.postservice.model.PostEntity;
import com.rdutta.postservice.repository.PostRepository;
import com.rdutta.postservice.service.PostService;
import com.rdutta.usermanagement.model.UserEntity;
import com.rdutta.usermanagement.repository.UserRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Service;


import java.time.LocalDateTime;
import java.util.List;
import java.util.concurrent.ThreadLocalRandom;
import java.util.stream.Collectors;

@Service
@Slf4j
public class PostServiceImpl implements PostService {
    private final UserRepository userRepository;
    private final PostRepository postRepository;
    private final KafkaTemplate<String, NewPostCreatedEvent> kafkaNewPostTemplate;
    private final KafkaTemplate<String, PostDeletedEvent> kafkaDeletePostTemplate;

    private final PostMapper postMapper;

    @Autowired
    public PostServiceImpl(PostRepository postRepository, KafkaTemplate<String, NewPostCreatedEvent> kafkaNewPostTemplate, KafkaTemplate<String, PostDeletedEvent> kafkaDeletePostTemplate, PostMapper postMapper,
                           UserRepository userRepository) {
        this.postRepository = postRepository;
        this.kafkaNewPostTemplate = kafkaNewPostTemplate;
        this.kafkaDeletePostTemplate = kafkaDeletePostTemplate;
        this.postMapper = postMapper;
        this.userRepository = userRepository;
    }

    // Create a new post
    @Override
    public void createPost(PostDTO postDTO) {
// Get the user entity from the repository using the user email in the postDTO
        UserEntity userEntity = userRepository.findBy_userEmail(postDTO.get_userEmail());

        // If user entity is null, throw an exception or handle it gracefully
        if (userEntity == null) {
            throw new IllegalArgumentException("User not found for the given email");
        }

        // Create a new PostEntity and set its attributes
        PostEntity postEntity = new PostEntity();
        postEntity.setUserEntity(userEntity); // set the user entity as a foreign key
        postEntity.set_title(postDTO.get_title());
        postEntity.set_body(postDTO.get_body());
        postEntity.set_userEmail(postDTO.get_userEmail());
        postEntity.onPostCreate();
        postEntity.setId(ThreadLocalRandom.current().nextLong(100000, 1000000));

        // Save the post entity to the database
        postRepository.save(postEntity);
        log.info("Post got posted from user - {} ", postDTO.get_userEmail());
        kafkaNewPostTemplate.send("post_created_topic", new NewPostCreatedEvent(postEntity.getId(), postEntity.getUserEntity().get_userEmail(), postEntity.get_title(), postEntity.get_body(), postEntity.get_postCreatedAt()));
    }

    // Retrieve all posts by user email
    @Override
    public List<PostDTO> getAllPostsByUser(String _userEmail) {
        List<PostEntity> postEntityList = postRepository.findBy_userEmail(_userEmail);
        return postEntityList.stream().map(postMapper::toDTO).collect(Collectors.toList());
    }

    @Override
    public PostDTO getPostById(Long id) {
        PostEntity postEntity = postRepository.findById(id).orElseThrow(()->new IllegalArgumentException("Post not found for the given id."));
        return postMapper.toDTO(postEntity);
    }

    @Override
    public void deletePost(Long id, String _userEmail) {
        // Get the user entity from the repository using the email parameter in the request
        UserEntity userEntity = userRepository.findBy_userEmail(_userEmail);

        // If user entity is null, throw an exception or handle it gracefully
        if (userEntity == null) {
            throw new IllegalArgumentException("User not found for the given email");
        }

        // Get the post entity from the repository using the post id parameter in the request
        PostEntity postEntity = postRepository.findById(id).orElseThrow(() -> new IllegalArgumentException("Post not found for the given id"));

        // Check if the user entity and post entity match
        if (!postEntity.get_userEmail().equals(_userEmail)) {
            throw new IllegalArgumentException("Post does not belong to the user");
        }

        PostDTO postDTO = new PostDTO();

        postDTO.set_postDeletedAt(LocalDateTime.now());

        // Delete the post entity using the post repository
        postRepository.deleteById(id);
        log.info("Post deleted with id -> {}", id);

        // Send a Kafka message to notify that the post has been deleted
        kafkaDeletePostTemplate.send("post_updated_topic", new PostDeletedEvent(postEntity.getId(), postEntity.get_userEmail(), postEntity.get_postDeletedAt()));
    }

}
