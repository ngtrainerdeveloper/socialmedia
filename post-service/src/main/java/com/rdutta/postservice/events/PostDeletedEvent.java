package com.rdutta.postservice.events;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class PostDeletedEvent {
    private Long _postId;
    private String _userEmail;
    private LocalDateTime _postDeletedAt;
}
