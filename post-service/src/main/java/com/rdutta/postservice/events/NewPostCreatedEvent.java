package com.rdutta.postservice.events;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class NewPostCreatedEvent {
    private Long _postId;
    private String _userEmail;
    private String _title;
    private String _body;
    private LocalDateTime createdAt;
}
