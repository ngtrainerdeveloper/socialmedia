package com.rdutta.postservice.service;

import com.rdutta.postservice.dto.PostDTO;

import java.util.List;

public interface PostService {
    void createPost(PostDTO postDTO);
    List<PostDTO> getAllPostsByUser(String _userEmail);
    PostDTO getPostById(Long id);

    void deletePost(Long id, String email);
}
