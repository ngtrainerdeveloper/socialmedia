package com.rdutta.postservice.mappers;

import com.rdutta.postservice.dto.PostDTO;
import com.rdutta.postservice.model.PostEntity;
import com.rdutta.usermanagement.model.UserEntity;
import org.springframework.stereotype.Component;

@Component
public class PostMapper {
    public PostEntity toEntity(PostDTO postDTO) {
        UserEntity user = new UserEntity();
        PostEntity postEntity = PostEntity.builder()
                .id(postDTO.getId())
                ._userEmail(user.get_userEmail())
                ._title(postDTO.get_title())
                ._body(postDTO.get_body())
                ._postCreatedAt(postDTO.get_postCreatedAt())
                ._postDeletedAt(postDTO.get_postDeletedAt())
                .build();
        return postEntity;
    }

    public PostDTO toDTO(PostEntity postEntity) {
        PostDTO postDTO = PostDTO.builder()
                .id(postEntity.getId())
                ._userEmail(postEntity.getUserEntity().get_userEmail())
                ._title(postEntity.get_title())
                ._body(postEntity.get_body())
                ._postCreatedAt(postEntity.get_postCreatedAt())
                ._postDeletedAt(postEntity.get_postDeletedAt())
                .build();
        return postDTO;
    }

}
